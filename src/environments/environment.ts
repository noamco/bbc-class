// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDl3h5KczqVrRebR5bc6SYIwvsg_BICy1E",
    authDomain: "bbcproject-f03f0.firebaseapp.com",
    databaseURL: "https://bbcproject-f03f0.firebaseio.com",
    projectId: "bbcproject-f03f0",
    storageBucket: "bbcproject-f03f0.appspot.com",
    messagingSenderId: "1078222236950",
    appId: "1:1078222236950:web:f7721b0ebb50484fe5f437"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
