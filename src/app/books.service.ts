import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Title } from '@angular/platform-browser';
import { NgForOf } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
books:any= [{Title:'Alice in Wonderland', Author:'Lewis Carrol'},{Title:'War and Peace', Author:'Leo Tolstoy'}, {Title:'The Magic Mountain', Author:'Thomas Mann'}]
  
// getBooks(){
//   const booksObservable=new Observable(
//     observer =>{
//       setInterval(
//         () =>observer.next(this.books),5000
//       );
//     }
//   )
//   return booksObservable;
// }
getBooks(){
  return this.db.collection('books').valueChanges({idField:`id`});
}
addBook(Title:string, Author:string){
  const book = {Title:Title,Author:Author}
  this.db.collection('books').add(book);
}
deleteBook(id:string){
  this.db.doc(`books/${id}`).delete();
}
updateBook(id:string,title:string,author:string){
  this.db.doc(`books/${id}`).update(
    {
      title:title,
      author:author
    }
  )
}
getBook(id:string):Observable<any>{
  return this.db.doc(`books/${id}`).get();
}
// addBooks(){
//   setInterval(()=> this.books.push({Title:"New Book",Author:"New Author"}),5000)
// }

constructor(private db:AngularFirestore) { }
}
