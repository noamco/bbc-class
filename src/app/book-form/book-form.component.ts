import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  constructor(
    private router: Router,private BooksService:BooksService,private route:ActivatedRoute) { }
      title:string;
      author:string;
      id:string;
      isEdit:Boolean=false;
      buttonText:string="Add Book";

      onSubmit(){ 
        if(this.isEdit){
          console.log('edit mode');
          this.BooksService.updateBook(this.id,this.title,this.author);
        } else {
          this.BooksService.addBook(this.title,this.author)
        }
        this.router.navigate(['/books']);  
      }  
    
      ngOnInit() {
        this.id = this.route.snapshot.params.id;
        if(this.id) {
        this.isEdit = true;
        this.buttonText = 'Update book'   
        this.BooksService.getBook(this.id).subscribe(
          book => {
            console.log(book.data().author)
            console.log(book.data().title)
            this.author = book.data().author;
            this.title = book.data().title; 
          }
        )
       }
      }
    }



